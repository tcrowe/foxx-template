// https://eslint.org/docs/user-guide/configuring/

const cfg = {
  env: {
    es6: true,
    node: true,
    browser: true,
    mocha: true
  },
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
    parser: "@babel/eslint-parser"
  },
  plugins: ["prettier", "babel"],
  extends: ["plugin:prettier/recommended", "eslint:recommended"],
  rules: {
    "prettier/prettier": ["error"],
    "no-console": ["off"],
    camelcase: ["off"]
  }
};

module.exports = cfg;
