require("dotenv/config");
const { createReadStream } = require("fs");
const { join } = require("path");
const isNil = require("lodash/isNil");
const isEmpty = require("lodash/isEmpty");
const isString = require("lodash/isString");
const get = require("lodash/get");
const debounce = require("lodash/debounce");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ZipWebpackPlugin = require("zip-webpack-plugin");
const { Database } = require("arangojs");
const chalk = require("chalk");
const urlJoin = require("url-join");
const manifest = require("./manifest.json");

const {
  NODE_ENV: mode,
  ARANGODB_URL,
  ARANGODB_DATABASE,
  ARANGODB_USER,
  ARANGODB_PASSWORD,
  ARANGODB_SERVICE_PATH
} = process.env;

const isDev = mode === "development";
const target = "node";
const devtool = false;
const ignored = [
  "**/.git/**",
  "**/node_modules/**",
  "**/.tmp/**",
  "**/dist/**"
];
const watchOptions = isDev ? { ignored } : undefined;
const tmpPath = join(__dirname, ".tmp");
const serviceZipName = "deploy-foxx-service.zip";
const serviceZipPath = join(tmpPath, serviceZipName);
const stats = "errors-only";

// https://arangodb.github.io/arangojs/7.3.0/modules/_database_.html#upgradeserviceoptions
const serviceOpts = {
  // set development = undefined for production or else it stays in dev
  development: isDev ? true : undefined,
  setup: isString(get(manifest, "scripts.setup")),
  teardown: isString(get(manifest, "scripts.teardown")),
  force: true
};

/**
 * Reusable error logger
 * @method reportError
 * @param {string} title
 * @returns {function}
 */
function reportError(title) {
  return function reportErrorInner(err) {
    const { message, stack } = err;
    console.error(rund(title));
    console.error(rund("message"), message);
    console.error(rund("stack"), stack);
  };
}

/*

Prepare arangodb

*/
const db = new Database({
  url: ARANGODB_URL,
  databaseName: ARANGODB_DATABASE
});

/**
 * Login to ArangoDB using .env username and password.
 * @method login
 */
async function login() {
  // the response is he tokem if needed
  await db.login(ARANGODB_USER, ARANGODB_PASSWORD);
  console.log(chalk.green("logged into arangodb"), "as", ARANGODB_USER);
}

// underline
const und = val => chalk.underline(val);

// red underline
const rund = val => chalk.red(und(val));

const reportLoginError = reportError("error logging into arangodb");

login().catch(function (err) {
  reportLoginError(err);
  process.exit(1);
});

/**
 * Webpack wants "./" first on some paths.
 * @method fixRelative
 * @param {string} path
 * @returns {string}
 */
const fixRelative = path => (path.startsWith("./") ? path : `./${path}`);

const mainRelPath = fixRelative(manifest.main);

// main and lifecycle scripts
const entry = {
  main: {
    import: mainRelPath,
    filename: mainRelPath
  }
};

/*

Each script or task a new entry is created.

*/
Object.keys(manifest.scripts || {}).forEach(function (key) {
  const relPath = fixRelative(manifest.scripts[key]);
  entry[`lifecycle-${key}`] = {
    import: relPath,
    filename: relPath
  };
});

/*

Each test is turned into an entry.

*/
(manifest.tests || []).forEach(function (testPath, index) {
  const testRelPath = fixRelative(testPath);
  entry[`tests-${index}`] = {
    import: testRelPath,
    filename: testRelPath
  };
});

const output = { libraryTarget: "commonjs2" };

// https://www.arangodb.com/docs/stable/appendix-java-script-modules.html#bundled-npm-modules
const externals = [
  // node compatibility built-ins
  /^assert$/,
  /^buffer$/,
  /^console$/,
  /^events$/,
  /^fs$/,
  /^module$/,
  /^path$/,
  /^punycode$/,
  /^querystring$/,
  /^stream$/,
  /^string_decoder$/,
  /^url$/,
  /^util$/,

  // arangodb extras
  /^@arangodb(\/|$)/,
  /^dedent$/,
  /^chai$/,
  /^error-stack-parser$/,
  /^graphql-sync$/,
  /^highlight.js$/,
  /^i$/,
  /^iconv-lite$/,
  /^joi$/,
  /^js-yaml$/,
  /^lodash/,
  /^minimatch$/,
  /^qs$/,
  /^semver$/,
  /^sinon$/,
  /^timezone$/
];

const reportServiceZipStreamError = reportError("error in service zip stream");
const reportServiceUpdateError = reportError("error updating foxx service");
const reportServiceTestsError = reportError("error running foxx service tests");

function testFoxxService() {
  if (isNil(manifest.tests) || manifest.tests.length === 0) {
    return;
  }
  db.runServiceTests(ARANGODB_SERVICE_PATH)
    .then(function ({ stats, tests, failures /*, pending, passes*/ }) {
      console.log(
        "suites",
        stats.suites,
        "tests",
        stats.tests,
        chalk.green("passes"),
        stats.passes,
        "pending",
        stats.pending,
        stats.failures > 0 ? chalk.red("failures") : "failures",
        stats.failures,
        "duration",
        `${stats.duration}ms`
      );
      const suites = {};
      tests.forEach(function ({ title, fullTitle, err /*, duration*/ }) {
        const t = title.trim();
        const suiteName = fullTitle.replace(` ${t}`, "").trim();
        if (isNil(suites[suiteName])) {
          suites[suiteName] = { ok: [], fail: [] };
        }
        if (isEmpty(err)) {
          suites[suiteName].ok.push(t);
        } else {
          suites[suiteName].fail.push(t);
        }
      });
      Object.keys(suites).forEach(function (key) {
        const { ok, fail } = suites[key];
        console.log(und(key), `${ok.length}/${ok.length + fail.length}`);
        if (ok.length > 0) {
          console.log(" ", chalk.green("  ok"), ok.join(", "));
        }
        if (fail.length > 0) {
          console.log(" ", chalk.red("fail"), fail.join(", "));
        }
      });
      if (failures.length > 0) {
        console.log(rund("failures:"));
        failures.forEach(function ({ fullTitle, err }) {
          console.log("---", fullTitle.trim(), "---");
          for (const key in err) {
            const val = err[key];
            const type = typeof val;
            const opstr = ["string", "boolean", "number"].includes(type)
              ? val
              : "";
            console.log(`${und(key)}:`, opstr);
          }
        });
      }
    })
    .catch(reportServiceTestsError);
}

function upgradeFoxxService(/*compilation*/) {
  const serviceZipStream = createReadStream(serviceZipPath);
  serviceZipStream.on("error", reportServiceZipStreamError);

  db.upgradeService(ARANGODB_SERVICE_PATH, serviceZipStream, serviceOpts)
    .then(function (res) {
      console.log(chalk.green("foxx service updated"), res.checksum);
      console.log(
        chalk.underline(
          urlJoin(ARANGODB_URL, "_db", ARANGODB_DATABASE, ARANGODB_SERVICE_PATH)
        )
      );
      console.log(chalk.underline(res.path));
      testFoxxService();
    })
    .catch(reportServiceUpdateError);
}

const plugins = [
  new CopyWebpackPlugin({
    patterns: ["manifest.json", { from: "readme.md", to: "README.md" }]
  }),

  // https://github.com/erikdesjardins/zip-webpack-plugin#readme
  new ZipWebpackPlugin({
    path: tmpPath,
    filename: serviceZipName
  }),

  // wait for finish and upload the service
  {
    apply: compiler =>
      compiler.hooks.afterEmit.tap(
        "AfterEmitPlugin",
        debounce(upgradeFoxxService, 100)
      )
  }
];

module.exports = {
  mode,
  target,
  devtool,
  entry,
  output,
  externals,
  plugins,
  watchOptions,
  stats
};
