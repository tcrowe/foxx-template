require("chai").should();
const { rootHandler } = require("../../src/index");

describe("unit tests", function () {
  it("rootHandler", function () {
    rootHandler(null, {
      json(op) {
        op.name.should.be.a("string");
      }
    });
  });
});
