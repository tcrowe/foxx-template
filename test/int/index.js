require("chai").should();
const request = require("@arangodb/request");
const { context } = require("@arangodb/locals");
const manifest = require("../../manifest.json");

const { baseUrl } = context;

describe("integration tests", function () {
  it("router initialized", function () {
    const res = request.get(baseUrl);
    res.statusCode.should.eql(200);
    res.body.should.be.a("string");
    res.body.should.include(manifest.name);
  });
});
