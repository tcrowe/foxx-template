# foxx-template

Quickly create an ArangoDB Foxx Microservice

## Table of Contents

+ [Setup](#setup)
+ [Development](#development)
+ [Production](#production)

## Setup

Clone this repository.

```sh
# install dependencies
npm install

# create your .env file
# customize the contents
cp .env-example .env
```

Customize [./manifest.json](./manifest.json) and \[./package.json].(./package.json) such as changing the name.

## Development

⚠ This will deploy the service to arangodb in `development` mode.

`npm run dev`

How does it work?

1. Build with webpack. A zip file is produced including [./README.md](./README.md) and [./manifest.json](./manifest.json).
2. Update the foxx service in arangodb via `arangojs` module.
3. Print the details to the console.

The output will give you details on how to access both the service HTTP API and the files where arangodb put them.

e.g.

<http://127.0.0.1:8529/_db/test/foxx-template>

`/opt/arangodb/js/apps/_db/test/foxx-template/APP`

## Production

⚠ This will deploy the service to arangodb in `production` mode.

`npm run prd`
