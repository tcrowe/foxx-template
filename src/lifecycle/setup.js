const isNil = require("lodash/isNil");
const { db } = require("@arangodb");

/*

Setup foxx-template collection

*/
const col1 = "foxx-template";

if (isNil(db._collection(col1))) {
  db._createDocumentCollection(col1);
}
