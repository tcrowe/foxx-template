const createRouter = require("@arangodb/foxx/router");
const { context } = require("@arangodb/locals");
const manifest = require("../manifest.json");

const router = createRouter();

function rootHandler(_, res) {
  res.json({
    name: manifest.name,
    description: manifest.description
  });
}

router
  .get("/", rootHandler)
  .summary(manifest.name)
  .description(manifest.description);

// context is similar to module.exports for foxx/arangodb
context.use(router);

// for testing
module.exports = { rootHandler };
