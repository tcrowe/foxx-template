/*

These are mostly the same as the defaults. The reason why I include them
instead of leaving the default is that some editors and other tools
have their own opinions and try to override the defaults. We just put them back
in here. -TC

*/

const prettierrc = {
  printWidth: 80,
  tabWidth: 2,
  useTabs: false,
  semi: true,
  singleQuote: false,
  trailingComma: "none",
  bracketSpacing: true,
  arrowParens: "avoid",
  proseWrap: "never",
  endOfLine: "lf"
};

module.exports = prettierrc;
